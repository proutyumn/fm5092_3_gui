﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FM5092_3_GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txtTryParse_TextChanged(object sender, EventArgs e)
        {
            double num = 0;
            if(double.TryParse(txtTryParse.Text, out num))
            {
                txtTryParse.BackColor = Color.White;
            }
            else
            {
                txtTryParse.BackColor = Color.HotPink;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            double tryparsenumber = 0;
            // i still have to read the data in the tryparse text box!
            try
            {
                tryparsenumber = Convert.ToDouble(txtTryParse.Text);
            }
            catch
            {
                MessageBox.Show("You didn't enter a number in the Tryparse box!");
            }
        }

        private void txtErrProv_TextChanged(object sender, EventArgs e)
        {
            double num;
            if(!double.TryParse(txtErrProv.Text, out num))
            {
                errorProvider1.SetError(txtErrProv, "Please enter a number!");
            }
            else
            {
                errorProvider1.SetError(txtErrProv, "");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            errorProvider1.SetIconAlignment(txtErrProv, ErrorIconAlignment.MiddleRight);
            errorProvider1.SetIconPadding(txtErrProv, 3);
        }
    }
}
