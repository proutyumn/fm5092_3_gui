﻿namespace FM5092_3_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtTryParse = new System.Windows.Forms.TextBox();
            this.lblTryParse = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtErrProv = new System.Windows.Forms.TextBox();
            this.lblErrProv = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTryParse
            // 
            this.txtTryParse.Location = new System.Drawing.Point(233, 73);
            this.txtTryParse.Name = "txtTryParse";
            this.txtTryParse.Size = new System.Drawing.Size(225, 22);
            this.txtTryParse.TabIndex = 0;
            this.txtTryParse.TextChanged += new System.EventHandler(this.txtTryParse_TextChanged);
            // 
            // lblTryParse
            // 
            this.lblTryParse.AutoSize = true;
            this.lblTryParse.Location = new System.Drawing.Point(153, 76);
            this.lblTryParse.Name = "lblTryParse";
            this.lblTryParse.Size = new System.Drawing.Size(74, 17);
            this.lblTryParse.TabIndex = 1;
            this.lblTryParse.Text = "Try Parse:";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(233, 281);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(225, 66);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "Submit!";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtErrProv
            // 
            this.txtErrProv.Location = new System.Drawing.Point(233, 128);
            this.txtErrProv.Name = "txtErrProv";
            this.txtErrProv.Size = new System.Drawing.Size(225, 22);
            this.txtErrProv.TabIndex = 3;
            this.txtErrProv.TextChanged += new System.EventHandler(this.txtErrProv_TextChanged);
            // 
            // lblErrProv
            // 
            this.lblErrProv.AutoSize = true;
            this.lblErrProv.Location = new System.Drawing.Point(126, 131);
            this.lblErrProv.Name = "lblErrProv";
            this.lblErrProv.Size = new System.Drawing.Size(101, 17);
            this.lblErrProv.TabIndex = 4;
            this.lblErrProv.Text = "Error Provider:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 416);
            this.Controls.Add(this.lblErrProv);
            this.Controls.Add(this.txtErrProv);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lblTryParse);
            this.Controls.Add(this.txtTryParse);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTryParse;
        private System.Windows.Forms.Label lblTryParse;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtErrProv;
        private System.Windows.Forms.Label lblErrProv;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

